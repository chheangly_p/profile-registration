//
//  ViewController.swift
//  Profile Registration
//
//  Created by Pathmazing on 8/7/19.
//  Copyright © 2019 Pathmazing Inc. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController {

    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var mPayImageView: UIImageView!
    @IBOutlet weak var idCardImageView: UIImageView!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var mPayButton: UIButton!
    @IBOutlet weak var idCarButton: UIButton!
    @IBOutlet weak var profileButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    
    var imagePickerState = ""
    var imagePicker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpImagePicker()
        hideImageView()
        scrollView.delegate = self
    }
    
    func hideImageView() {
        idCardImageView.isHidden = true
        profileImageView.isHidden = true
        mPayImageView.isHidden = true
    }
    
    func setUpImagePicker() {
        imagePicker.delegate = self
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            imagePicker.sourceType = .photoLibrary
            imagePicker.allowsEditing = true
            imagePicker.mediaTypes = ["public.image"]
        }
    }
    
    @IBAction func onIDCardButtonClick(_ sender: Any) {
        imagePickerState = "IDCard"
        present(imagePicker, animated: true, completion: nil)
    }
    
    @IBAction func onProfileButtonClick(_ sender: Any) {
        imagePickerState = "Profile"
        present(imagePicker, animated: true, completion: nil)
    }
    
    @IBAction func onMPayButtonClick(_ sender: Any) {
        imagePickerState = "MPay"
        present(imagePicker, animated: true, completion: nil)
    }
    
}

extension RegisterViewController: UIImagePickerControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let image = info[.editedImage] as! UIImage
        if imagePickerState == "MPay" {
            mPayImageView.isHidden = false
            mPayImageView.image = image
        } else if imagePickerState == "Profile" {
            profileImageView.isHidden = false
            profileImageView.image = image
        } else if imagePickerState == "IDCard" {
            idCardImageView.isHidden = false
            idCardImageView.image = image
        }
        dismiss(animated: true, completion: nil)
    }
    
}

extension RegisterViewController: UINavigationControllerDelegate {
    
}

extension RegisterViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        UIView.animate(withDuration: 0.5) {
//            self.titleLabel.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
            self.topView.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
        }
    }
    
}
