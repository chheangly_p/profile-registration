//
//  UIButtonEx.swift
//  Profile Registration
//
//  Created by Pathmazing on 8/7/19.
//  Copyright © 2019 Pathmazing Inc. All rights reserved.
//

import UIKit

extension UIButton {
    @IBInspectable
    var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
        }
    }
}
